//
//  ViewController.m
//  Test for Bitbucket
//
//  Created by Владислав on 02.12.16.
//  Copyright © 2016 Vladislav Popov. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    //some changings here
    
    //First change from master.
    
    //Second change from master.
    
    //Third change from master.
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    //Fourth change from master.
    
    //Fifth change from master.
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
